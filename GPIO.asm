
_initInOut:

;GPIO.c,5 :: 		void initInOut(void)
;GPIO.c,7 :: 		TRISC = 0;    // PORT C OUTPUT PWM AND GPIO
	CLRF       TRISC+0
;GPIO.c,8 :: 		PORTC = 0;
	CLRF       PORTC+0
;GPIO.c,9 :: 		PWM1_Init(ONEKHZ);
	BCF        T2CON+0, 0
	BCF        T2CON+0, 1
	MOVLW      -1
	MOVWF      PR2+0
	CALL       _PWM1_Init+0
;GPIO.c,10 :: 		PWM2_Init(ONEKHZ);
	BCF        T2CON+0, 0
	BCF        T2CON+0, 1
	MOVLW      -1
	MOVWF      PR2+0
	CALL       _PWM2_Init+0
;GPIO.c,11 :: 		TRISD = 0xFF;   // PORT D INPUT
	MOVLW      255
	MOVWF      TRISD+0
;GPIO.c,12 :: 		}
L_end_initInOut:
	RETURN
; end of _initInOut

_readLine:

;GPIO.c,13 :: 		void readLine(void)
;GPIO.c,15 :: 		in1 = PORTD.F0;
	MOVLW      0
	BTFSC      PORTD+0, 0
	MOVLW      1
	MOVWF      _in1+0
	CLRF       _in1+1
;GPIO.c,16 :: 		in2 = PORTD.F1;
	MOVLW      0
	BTFSC      PORTD+0, 1
	MOVLW      1
	MOVWF      _in2+0
	CLRF       _in2+1
;GPIO.c,17 :: 		in3 = PORTD.F2;
	MOVLW      0
	BTFSC      PORTD+0, 2
	MOVLW      1
	MOVWF      _in3+0
	CLRF       _in3+1
;GPIO.c,18 :: 		in4 = PORTD.F3;
	MOVLW      0
	BTFSC      PORTD+0, 3
	MOVLW      1
	MOVWF      _in4+0
	CLRF       _in4+1
;GPIO.c,19 :: 		in5 = PORTD.F4;
	MOVLW      0
	BTFSC      PORTD+0, 4
	MOVLW      1
	MOVWF      _in5+0
	CLRF       _in5+1
;GPIO.c,20 :: 		in6 = PORTD.F5;
	MOVLW      0
	BTFSC      PORTD+0, 5
	MOVLW      1
	MOVWF      _in6+0
	CLRF       _in6+1
;GPIO.c,21 :: 		in7 = PORTD.F6;
	MOVLW      0
	BTFSC      PORTD+0, 6
	MOVLW      1
	MOVWF      _in7+0
	CLRF       _in7+1
;GPIO.c,22 :: 		}
L_end_readLine:
	RETURN
; end of _readLine

_motorL:

;GPIO.c,24 :: 		void motorL(int pwm)
;GPIO.c,26 :: 		PWM1_Set_Duty(pwm);
	MOVF       FARG_motorL_pwm+0, 0
	MOVWF      FARG_PWM1_Set_Duty_new_duty+0
	CALL       _PWM1_Set_Duty+0
;GPIO.c,27 :: 		PORTC.F4 = 0;
	BCF        PORTC+0, 4
;GPIO.c,28 :: 		PWM1_Start();
	CALL       _PWM1_Start+0
;GPIO.c,29 :: 		}
L_end_motorL:
	RETURN
; end of _motorL

_motorR:

;GPIO.c,31 :: 		void motorR(int pwm)
;GPIO.c,33 :: 		PWM2_Set_Duty(pwm);
	MOVF       FARG_motorR_pwm+0, 0
	MOVWF      FARG_PWM2_Set_Duty_new_duty+0
	CALL       _PWM2_Set_Duty+0
;GPIO.c,34 :: 		PORTC.F0 = 0;
	BCF        PORTC+0, 0
;GPIO.c,35 :: 		PWM2_Start();
	CALL       _PWM2_Start+0
;GPIO.c,36 :: 		}
L_end_motorR:
	RETURN
; end of _motorR

_main:

;GPIO.c,37 :: 		void main() {
;GPIO.c,40 :: 		initInOut();
	CALL       _initInOut+0
;GPIO.c,41 :: 		while(1)
L_main0:
;GPIO.c,43 :: 		readLine();
	CALL       _readLine+0
;GPIO.c,45 :: 		if(in7 == 0 && in6 == 1)                 // diem vat can    OK
	MOVLW      0
	XORWF      _in7+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main52
	MOVLW      0
	XORWF      _in7+0, 0
L__main52:
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	MOVLW      0
	XORWF      _in6+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main53
	MOVLW      1
	XORWF      _in6+0, 0
L__main53:
	BTFSS      STATUS+0, 2
	GOTO       L_main4
L__main46:
;GPIO.c,47 :: 		motorL(0);
	CLRF       FARG_motorL_pwm+0
	CLRF       FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,48 :: 		motorR(0);
	CLRF       FARG_motorR_pwm+0
	CLRF       FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,49 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;GPIO.c,50 :: 		motorL(150);
	MOVLW      150
	MOVWF      FARG_motorL_pwm+0
	CLRF       FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,51 :: 		motorR(0);
	CLRF       FARG_motorR_pwm+0
	CLRF       FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,52 :: 		Delay_ms(3500);
	MOVLW      36
	MOVWF      R11+0
	MOVLW      131
	MOVWF      R12+0
	MOVLW      207
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
;GPIO.c,53 :: 		checkpoint = 1;
	MOVLW      1
	MOVWF      _checkpoint+0
	MOVLW      0
	MOVWF      _checkpoint+1
;GPIO.c,54 :: 		}
L_main4:
;GPIO.c,56 :: 		if((in1 == 0 && in3 == 0) || (in3 == 0 && in5 == 0) || (in1 == 0 && in3 == 0 && in5 == 0))  // diem re 90*
	MOVLW      0
	XORWF      _in1+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main54
	MOVLW      0
	XORWF      _in1+0, 0
L__main54:
	BTFSS      STATUS+0, 2
	GOTO       L__main45
	MOVLW      0
	XORWF      _in3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main55
	MOVLW      0
	XORWF      _in3+0, 0
L__main55:
	BTFSS      STATUS+0, 2
	GOTO       L__main45
	GOTO       L__main42
L__main45:
	MOVLW      0
	XORWF      _in3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main56
	MOVLW      0
	XORWF      _in3+0, 0
L__main56:
	BTFSS      STATUS+0, 2
	GOTO       L__main44
	MOVLW      0
	XORWF      _in5+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main57
	MOVLW      0
	XORWF      _in5+0, 0
L__main57:
	BTFSS      STATUS+0, 2
	GOTO       L__main44
	GOTO       L__main42
L__main44:
	MOVLW      0
	XORWF      _in1+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main58
	MOVLW      0
	XORWF      _in1+0, 0
L__main58:
	BTFSS      STATUS+0, 2
	GOTO       L__main43
	MOVLW      0
	XORWF      _in3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main59
	MOVLW      0
	XORWF      _in3+0, 0
L__main59:
	BTFSS      STATUS+0, 2
	GOTO       L__main43
	MOVLW      0
	XORWF      _in5+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main60
	MOVLW      0
	XORWF      _in5+0, 0
L__main60:
	BTFSS      STATUS+0, 2
	GOTO       L__main43
	GOTO       L__main42
L__main43:
	GOTO       L_main15
L__main42:
;GPIO.c,58 :: 		if(checkpoint == 1)         // point 1 OK
	MOVLW      0
	XORWF      _checkpoint+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main61
	MOVLW      1
	XORWF      _checkpoint+0, 0
L__main61:
	BTFSS      STATUS+0, 2
	GOTO       L_main16
;GPIO.c,60 :: 		motorL(0);
	CLRF       FARG_motorL_pwm+0
	CLRF       FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,61 :: 		motorR(0);
	CLRF       FARG_motorR_pwm+0
	CLRF       FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,62 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_main17:
	DECFSZ     R13+0, 1
	GOTO       L_main17
	DECFSZ     R12+0, 1
	GOTO       L_main17
	DECFSZ     R11+0, 1
	GOTO       L_main17
	NOP
;GPIO.c,63 :: 		motorL(0);
	CLRF       FARG_motorL_pwm+0
	CLRF       FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,64 :: 		motorR(150);
	MOVLW      150
	MOVWF      FARG_motorR_pwm+0
	CLRF       FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,65 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_main18:
	DECFSZ     R13+0, 1
	GOTO       L_main18
	DECFSZ     R12+0, 1
	GOTO       L_main18
	DECFSZ     R11+0, 1
	GOTO       L_main18
	NOP
;GPIO.c,66 :: 		motorL(0);
	CLRF       FARG_motorL_pwm+0
	CLRF       FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,67 :: 		motorR(0);
	CLRF       FARG_motorR_pwm+0
	CLRF       FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,68 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_main19:
	DECFSZ     R13+0, 1
	GOTO       L_main19
	DECFSZ     R12+0, 1
	GOTO       L_main19
	DECFSZ     R11+0, 1
	GOTO       L_main19
	NOP
;GPIO.c,70 :: 		checkpoint = 2;
	MOVLW      2
	MOVWF      _checkpoint+0
	MOVLW      0
	MOVWF      _checkpoint+1
;GPIO.c,71 :: 		}
L_main16:
;GPIO.c,74 :: 		}
L_main15:
;GPIO.c,76 :: 		if(in3 == 0 && in4 == 0 && in5 == 0 && checkpoint == 2)  // diem re 90*   thu 2
	MOVLW      0
	XORWF      _in3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main62
	MOVLW      0
	XORWF      _in3+0, 0
L__main62:
	BTFSS      STATUS+0, 2
	GOTO       L_main22
	MOVLW      0
	XORWF      _in4+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main63
	MOVLW      0
	XORWF      _in4+0, 0
L__main63:
	BTFSS      STATUS+0, 2
	GOTO       L_main22
	MOVLW      0
	XORWF      _in5+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main64
	MOVLW      0
	XORWF      _in5+0, 0
L__main64:
	BTFSS      STATUS+0, 2
	GOTO       L_main22
	MOVLW      0
	XORWF      _checkpoint+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main65
	MOVLW      2
	XORWF      _checkpoint+0, 0
L__main65:
	BTFSS      STATUS+0, 2
	GOTO       L_main22
L__main41:
;GPIO.c,79 :: 		motorL(0);
	CLRF       FARG_motorL_pwm+0
	CLRF       FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,80 :: 		motorR(0);
	CLRF       FARG_motorR_pwm+0
	CLRF       FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,81 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main23:
	DECFSZ     R13+0, 1
	GOTO       L_main23
	DECFSZ     R12+0, 1
	GOTO       L_main23
	DECFSZ     R11+0, 1
	GOTO       L_main23
	NOP
	NOP
;GPIO.c,82 :: 		motorL(150);
	MOVLW      150
	MOVWF      FARG_motorL_pwm+0
	CLRF       FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,83 :: 		motorR(0);
	CLRF       FARG_motorR_pwm+0
	CLRF       FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,84 :: 		Delay_ms(3000);
	MOVLW      31
	MOVWF      R11+0
	MOVLW      113
	MOVWF      R12+0
	MOVLW      30
	MOVWF      R13+0
L_main24:
	DECFSZ     R13+0, 1
	GOTO       L_main24
	DECFSZ     R12+0, 1
	GOTO       L_main24
	DECFSZ     R11+0, 1
	GOTO       L_main24
	NOP
;GPIO.c,85 :: 		checkpoint = 3;
	MOVLW      3
	MOVWF      _checkpoint+0
	MOVLW      0
	MOVWF      _checkpoint+1
;GPIO.c,86 :: 		}
L_main22:
;GPIO.c,88 :: 		if(in1 == 1 && in3 == 1 && in5 == 1)       // Khong line
	MOVLW      0
	XORWF      _in1+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main66
	MOVLW      1
	XORWF      _in1+0, 0
L__main66:
	BTFSS      STATUS+0, 2
	GOTO       L_main27
	MOVLW      0
	XORWF      _in3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main67
	MOVLW      1
	XORWF      _in3+0, 0
L__main67:
	BTFSS      STATUS+0, 2
	GOTO       L_main27
	MOVLW      0
	XORWF      _in5+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main68
	MOVLW      1
	XORWF      _in5+0, 0
L__main68:
	BTFSS      STATUS+0, 2
	GOTO       L_main27
L__main40:
;GPIO.c,90 :: 		motorL(100);
	MOVLW      100
	MOVWF      FARG_motorL_pwm+0
	MOVLW      0
	MOVWF      FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,91 :: 		motorR(100);
	MOVLW      100
	MOVWF      FARG_motorR_pwm+0
	MOVLW      0
	MOVWF      FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,92 :: 		}
L_main27:
;GPIO.c,94 :: 		if(in1 == 1 && in3 == 0 && in5 == 1)       // Line giua
	MOVLW      0
	XORWF      _in1+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main69
	MOVLW      1
	XORWF      _in1+0, 0
L__main69:
	BTFSS      STATUS+0, 2
	GOTO       L_main30
	MOVLW      0
	XORWF      _in3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main70
	MOVLW      0
	XORWF      _in3+0, 0
L__main70:
	BTFSS      STATUS+0, 2
	GOTO       L_main30
	MOVLW      0
	XORWF      _in5+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main71
	MOVLW      1
	XORWF      _in5+0, 0
L__main71:
	BTFSS      STATUS+0, 2
	GOTO       L_main30
L__main39:
;GPIO.c,96 :: 		motorL(100);
	MOVLW      100
	MOVWF      FARG_motorL_pwm+0
	MOVLW      0
	MOVWF      FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,97 :: 		motorR(100);
	MOVLW      100
	MOVWF      FARG_motorR_pwm+0
	MOVLW      0
	MOVWF      FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,98 :: 		}
L_main30:
;GPIO.c,100 :: 		if(in1 == 0 &&  in3 == 1 && in5 == 1)      // Line trai
	MOVLW      0
	XORWF      _in1+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main72
	MOVLW      0
	XORWF      _in1+0, 0
L__main72:
	BTFSS      STATUS+0, 2
	GOTO       L_main33
	MOVLW      0
	XORWF      _in3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main73
	MOVLW      1
	XORWF      _in3+0, 0
L__main73:
	BTFSS      STATUS+0, 2
	GOTO       L_main33
	MOVLW      0
	XORWF      _in5+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main74
	MOVLW      1
	XORWF      _in5+0, 0
L__main74:
	BTFSS      STATUS+0, 2
	GOTO       L_main33
L__main38:
;GPIO.c,102 :: 		motorL(0);
	CLRF       FARG_motorL_pwm+0
	CLRF       FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,103 :: 		motorR(150);
	MOVLW      150
	MOVWF      FARG_motorR_pwm+0
	CLRF       FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,104 :: 		}
L_main33:
;GPIO.c,106 :: 		if(in1 == 1 && in3 == 1 && in5 == 0)       // Line phai
	MOVLW      0
	XORWF      _in1+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main75
	MOVLW      1
	XORWF      _in1+0, 0
L__main75:
	BTFSS      STATUS+0, 2
	GOTO       L_main36
	MOVLW      0
	XORWF      _in3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main76
	MOVLW      1
	XORWF      _in3+0, 0
L__main76:
	BTFSS      STATUS+0, 2
	GOTO       L_main36
	MOVLW      0
	XORWF      _in5+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main77
	MOVLW      0
	XORWF      _in5+0, 0
L__main77:
	BTFSS      STATUS+0, 2
	GOTO       L_main36
L__main37:
;GPIO.c,108 :: 		motorL(150);
	MOVLW      150
	MOVWF      FARG_motorL_pwm+0
	CLRF       FARG_motorL_pwm+1
	CALL       _motorL+0
;GPIO.c,109 :: 		motorR(0);
	CLRF       FARG_motorR_pwm+0
	CLRF       FARG_motorR_pwm+1
	CALL       _motorR+0
;GPIO.c,112 :: 		}
L_main36:
;GPIO.c,117 :: 		}
	GOTO       L_main0
;GPIO.c,118 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
