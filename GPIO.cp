#line 1 "C:/Users/W530/Desktop/GPIO/pic_line/GPIO.c"

int in1, in2, in3, in4, in5, in6, in7;
int checkpoint;

void initInOut(void)
{
 TRISC = 0;
 PORTC = 0;
 PWM1_Init( 1111101000 );
 PWM2_Init( 1111101000 );
 TRISD = 0xFF;
}
void readLine(void)
{
 in1 = PORTD.F0;
 in2 = PORTD.F1;
 in3 = PORTD.F2;
 in4 = PORTD.F3;
 in5 = PORTD.F4;
 in6 = PORTD.F5;
 in7 = PORTD.F6;
}

void motorL(int pwm)
{
 PWM1_Set_Duty(pwm);
 PORTC.F4 = 0;
 PWM1_Start();
}

void motorR(int pwm)
{
 PWM2_Set_Duty(pwm);
 PORTC.F0 = 0;
 PWM2_Start();
}
void main() {


 initInOut();
 while(1)
 {
 readLine();

 if(in7 == 0 && in6 == 1)
 {
 motorL(0);
 motorR(0);
 Delay_ms(2000);
 motorL(150);
 motorR(0);
 Delay_ms(3500);
 checkpoint = 1;
 }

 if((in1 == 0 && in3 == 0) || (in3 == 0 && in5 == 0) || (in1 == 0 && in3 == 0 && in5 == 0))
 {
 if(checkpoint == 1)
 {
 motorL(0);
 motorR(0);
 Delay_ms(2000);
 motorL(0);
 motorR(150);
 Delay_ms(2000);
 motorL(0);
 motorR(0);
 Delay_ms(2000);

 checkpoint = 2;
 }


 }

if(in3 == 0 && in4 == 0 && in5 == 0 && checkpoint == 2)
 {

 motorL(0);
 motorR(0);
 Delay_ms(1000);
 motorL(150);
 motorR(0);
 Delay_ms(3000);
 checkpoint = 3;
 }

 if(in1 == 1 && in3 == 1 && in5 == 1)
 {
 motorL(100);
 motorR(100);
 }

 if(in1 == 1 && in3 == 0 && in5 == 1)
 {
 motorL(100);
 motorR(100);
 }

 if(in1 == 0 && in3 == 1 && in5 == 1)
 {
 motorL(0);
 motorR(150);
 }

 if(in1 == 1 && in3 == 1 && in5 == 0)
 {
 motorL(150);
 motorR(0);


 }




 }
}
